﻿# Проект производственной практики

*Репозиторий проекта производственной практики студентов группы ИВТ-19*

---

## Содержание
[Установка CMDBuild](#install)
- [Установка CMDBuild через графический интерфейс (Windows и Linux)](#install-win-lin)
- [Установка CMDBuild READY2USE через docker-контейнер (Linux)](#install-lin)
    - [1. Установка Docker](#docker-install)
    - [2. Настройка команды Docker без sudo (необязательно)](#docker-sudo)
    - [3. Установка docker-контейнера CMDBuild READY2USE](#docker-container)
    - [4. Управление контейнерами Docker](#docker-control)
[Запуск CMDBuild](#run)

## Установка CMDBuild <a name="install"></a>

### Установка CMDBuild через графический интерфейс (Windows и Linux) <a name="install-win-lin"></a>

Чтобы упростить стандартную установку и настройку CMDBuild, в пошаговой установке с графическим интерфейсом был предоставлен как для операционных систем Windows, так и для Unix.

После загрузки файла .war откройте терминал (в Linux) или окно cmd (в Windows) и перейдите в папку, содержащую файл .war. Чтобы запустить графический интерфейс, введите следующую команду:

```sh
java -jar cmdbuild.war -v
```

Нажав «install CMDBuild V3», мы перейдем к первым шагам настройки.

![Drag Racing](https://programmersought.com/images/591/ea30c89d656f3ee120709b949d7d518f.png)

В этой форме мы можем определить базовую конфигурацию CMDBuild. Описание полей:

- Tomcat dir: путь, по которому можно установить свежий tomcat
- Tomcat port: порт, который Tomcat будет использовать для связи
- Database type: представлены две базовые базы данных, пустая, в которой представлена только базовая структура, но данные не добавлены, или демонстрационная, в которой, помимо базовой структуры, некоторые данные добавлены в демонстрационных целях.
- Database host: хост, на котором устанавливается tomcat, localhost, если мы хотим запустить его на нашем компьютере.
- Database port: порт, используемый для связи с базой данных.
- Database name: имя создаваемой базы данных.
- Database user: основной пользователь базы данных, по умолчанию: cmdbuild.
- Database password: основной пароль пользователя базы данных, по умолчанию: cmdbuild.
- Database admin user: пользователь-администратор базы данных, по умолчанию: postgres.
- Database admin password: пароль администратора базы данных, по умолчанию: postgres.

Хорошая практика - протестировать конфигурацию с помощью кнопки «Test Config».

Теперь мы можем продолжить, нажав кнопку «INSTALL».

Будут установлены Apache tomcat и CMDBuild, также будет загружена выбранная база данных.

Как только установка будет завершена, появится всплывающее окно с вопросом, хотим ли мы запускать приложение или нет.

При выборе «yes» Apache tomcat загрузится, и установщик уведомит нас, как только приложение будет готово к использованию.

### Установка CMDBuild READY2USE через docker-контейнер (Linux) <a name="install-lin"></a>

#### 1. Установка Docker <a name="docker-install"></a>
Первым делом обновите существующий список пакетов:

```sh
sudo apt update
```

Затем установите несколько необходимых пакетов, которые позволяют apt использовать пакеты через HTTPS:

```sh
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

Добавьте ключ GPG для официального репозитория Docker в вашу систему:

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Добавьте репозиторий Docker в источники APT:

```sh
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
```

Потом обновите базу данных пакетов и добавьте в нее пакеты Docker из недавно добавленного репозитория:

```sh
sudo apt update
```

Убедитесь, что установка будет выполняться из репозитория Docker, а не из репозитория Ubuntu по умолчанию:

```sh
apt-cache policy docker-ce
```

Вы должны получить следующий вывод, хотя номер версии Docker может отличаться:
```sh
docker-ce:
  Installed: (none)
  Candidate: 5:19.03.9~3-0~ubuntu-focal
  Version table:
     5:19.03.9~3-0~ubuntu-focal 500
        500 https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
```

Установите Docker:

```sh
sudo apt install docker-ce
```

Docker должен быть установлен, демон-процесс запущен, а для процесса активирован запуск при загрузке. Проверьте, что он запущен:

```sh
sudo systemctl status docker
```

Вывод должен выглядеть примерно следующим образом, указывая, что служба активна и запущена:

```sh
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2020-05-19 17:00:41 UTC; 17s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 24321 (dockerd)
      Tasks: 8
     Memory: 46.4M
     CGroup: /system.slice/docker.service
             └─24321 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```

После установки Docker у вас будет доступ не только к службе Docker (демон-процесс), но и к утилите командной строки docker или клиенту Docker.

#### 2. Настройка команды Docker без sudo (необязательно) <a name="docker-sudo"></a>

По умолчанию команда docker может быть запущена только пользователем root или пользователем из группы docker, которая автоматически создается при установке Docker. Если вы попытаетесь запустить команду docker без префикса sudo или с помощью пользователя, который не находится в группе docker, то получите следующий вывод:

```sh
docker: Cannot connect to the Docker daemon. Is the docker daemon running on this host?.
See 'docker run --help'.
```

Если вы не хотите каждый раз вводить sudo при запуске команды docker, добавьте свое имя пользователя в группу docker:

```sh
sudo usermod -aG docker ${USER}
```

Чтобы применить добавление нового члена группы, выйдите и войдите на сервер или введите следующее:

```sh
su - ${USER}
```

Вы должны будете ввести пароль вашего пользователя, чтобы продолжить.

Проверьте, что ваш пользователь добавлен в группу docker, введя следующее:

```sh
id -nG
```

Если всё сделано верно, то получите следующий вывод:
```sh
sammy sudo docker
```

#### 3. Установка docker-контейнера CMDBuild READY2USE <a name="docker-container"></a>

Для установки готового решения CMDBuild READY2USE необходимо выполнить 2 команды:
```sh
docker run --name cmdbuild_db -p 5432:5432 -d itmicus/cmdbuild:db-3.0
```
```sh
docker run --name cmdbuild_app --restart unless-stopped -e CMDBUILD_DUMP="demo.dump.xz" --link cmdbuild_db  -p 8090:8080 -d itmicus/cmdbuild:r2u-2.0-3.2
```

#### 4. Управление контейнерами Docker <a name="docker-control"></a>

Чтобы просмотреть активные контейнеры, используйте следующую команду:
```sh
docker ps
```

Чтобы просмотреть все контейнеры — активные и неактивные, воспользуйтесь командой docker ps с переключателем -a:
```sh
docker ps -a
```

Чтобы запустить остановленный контейнер, воспользуйтесь docker start с идентификатором контейнера или именем контейнера. Например:
```sh
docker start 1c08a7a0d0e4
```
где `1c08a7a0d0e4` - идентификатор контейнера

Контейнер будет запущен, а вы сможете использовать docker ps, чтобы просматривать его статус:
```sh
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
1c08a7a0d0e4        ubuntu              "/bin/bash"         3 minutes ago       Up 5 seconds                            quizzical_mcnulty
```

## Запуск CMDBuild <a name="run"></a>

Подключение к системе CMDBuild осуществляется путём открытия в браузере адреса http://localhost:8090/cmdbuild

Данные для входа по умолчанию: 
- Имя пользователя: admin
- Пароль: admin.
